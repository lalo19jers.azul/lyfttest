// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {

    val kotlin_version by extra("1.5.20")
    repositories {
        google()
        jcenter()
    }

    dependencies {
        classpath (AndroidTools.CLASS_PATH_ANDROID_TOOLS_GRADLE)
        classpath(Kotlin.GRADLE_PLUGIN)
        classpath (Dependencies.NAVIGATION_SAFE_ARGS)
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
    }

}

allprojects {

    repositories {
        google()
        jcenter()
        maven { setUrl("https://jitpack.io") }
    }

}

tasks.register("clean", Delete::class.java) {
    delete(rootProject.buildDir)
}