package com.mx.lyfttest.di.presentation

import com.mx.lyfttest.presentation.addClient.CreateClientViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val addClientModule = module {
    viewModel { CreateClientViewModel(addClient = get()) }
}