package com.mx.lyfttest.di.presentation

import com.mx.lyfttest.presentation.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeModule = module {
    viewModel { HomeViewModel(getClients = get()) }
}