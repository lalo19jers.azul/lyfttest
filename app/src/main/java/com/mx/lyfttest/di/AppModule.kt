package com.mx.lyfttest.di

import com.mx.clients.clientsModule
import com.mx.datasource.httpClientModule
import com.mx.lyfttest.LyftApp
import com.mx.lyfttest.di.presentation.addClientModule
import com.mx.lyfttest.di.presentation.homeModule
import com.mx.network.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.core.logger.Level
/**
 * This class is our dependency injection. Built with koin.
 * Our App architecture is Clean Architecture + MVVM.
 * And we change the Viewmodel factory with this dependency injection.
 * If you need more parameters will be welcome in this module.
 */
fun LyftApp.initKoin() {
    startKoin {
        val modules = getPresentationModules() + getSharedModules() + getFeatureModules()
        androidLogger(Level.ERROR)
        androidContext(applicationContext)
        modules(modules)
    }
}

/**
 *
 * @return [List]
 */
private fun getSharedModules(): List<Module> = listOf(
    /** **/
    networkModule,
    httpClientModule
)

/**
 *
 * @return [List]
 */
private fun getFeatureModules(): List<Module> = listOf(
    clientsModule
)

/**
 * Presentation module is the layer that interacts with UI.
 * Presentation layer contains ViewModel, Fragments and Activities.
 * @return [List]
 */
private fun getPresentationModules(): List<Module> = listOf(
    /**  **/
    homeModule,
    addClientModule

)