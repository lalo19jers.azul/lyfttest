package com.mx.lyfttest.presentation.home

import androidx.lifecycle.ViewModel
import com.mx.clients.presentation.getClients.GetClients

class HomeViewModel(
    getClients: GetClients
) : ViewModel(), GetClients by getClients