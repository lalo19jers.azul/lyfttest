package com.mx.lyfttest.presentation.addClient

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.mx.clients.domain.useCase.addClient.AddClientParams
import com.mx.clients.presentation.addClient.AddClientStatus
import com.mx.domain.Status
import com.mx.lyfttest.R
import com.mx.lyfttest.databinding.CreateClientFragmentBinding
import com.mx.lyfttest.presentation.common.extensions.hideProgressBar
import com.mx.lyfttest.presentation.common.extensions.presentShortSnackBar
import com.mx.lyfttest.presentation.common.extensions.showProgressBar
import kotlinx.serialization.SerialName
import kotlinx.serialization.json.JsonObject
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel

class CreateClientFragment : Fragment() {

    /* */
    private val binding: CreateClientFragmentBinding
            by lazy { CreateClientFragmentBinding.inflate(layoutInflater) }


    private val viewModel: CreateClientViewModel by viewModel()

    /** */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    /** */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
    }

    /** */
    private fun setUpView() {
        binding.apply {
            acbCreateClient.setOnClickListener(::addClient)
        }
    }

    /** */
    private fun addClient(view: View) {
        binding.apply {
            val name = etName.text.toString()
            val lastName = etLastname.text.toString()
            val phone = etPhone.text.toString()
            val email = etEmail.text.toString()


            viewModel.addClientAsLiveData(
                params = AddClientParams(
                    name, lastName, email, phone
                )
            ).observe(viewLifecycleOwner, handleAddClientObserver())
        }

    }


    /** */
    private fun handleAddClientObserver() = Observer<AddClientStatus> {
        requireContext().hideProgressBar()
        when (it) {
            is Status.Loading -> requireContext().showProgressBar()
            is Status.Error -> binding.root.presentShortSnackBar(it.failure.toString())
            is Status.Done -> binding.root.presentShortSnackBar("Cliente creado exitosamente")
        }
    }

}