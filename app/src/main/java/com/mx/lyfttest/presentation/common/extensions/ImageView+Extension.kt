
package com.mx.lyfttest.presentation.common.extensions

import android.widget.ImageView
import coil.api.load
import coil.size.Scale
import com.mx.lyfttest.R

/** */
fun ImageView.loadImage(url: String?) {
    url?.let {
        if (it.isValidUrl())
            this.load(url) {
                error(R.drawable.ic_launcher_background)
                scale(Scale.FIT)
            }
        else load(R.drawable.ic_launcher_foreground)
    }

}
