package com.mx.lyfttest.presentation.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mx.clients.domain.entity.Client
import com.mx.lyfttest.databinding.ItemClientBinding

class ClientViewHolder(
    private val binding: ItemClientBinding
): RecyclerView.ViewHolder(binding.root) {

    /** */
    fun bind(client: Client, onRowClick: (Client) -> Unit) {
        binding.run {
            tvName.text = client.name.plus(" ${client.lastName}").replace("\"", "")
            tvAddress.text = client.address?.replace("\"", "")
            tvPhoneEmal.text = client.email.plus(" / ${client.phone}").replace("\"", "")
            tvClabe.text = client.clabe?.replace("\"", "")
            root.setOnClickListener { onRowClick(client) }
        }
    }

    /**
     * Inflates item
     */
    companion object {
        fun from(parent: ViewGroup): ClientViewHolder {
            val layoutInflater = ItemClientBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
            return ClientViewHolder(layoutInflater)
        }
    }
}