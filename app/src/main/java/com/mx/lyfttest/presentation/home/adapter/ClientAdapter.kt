package com.mx.lyfttest.presentation.home.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.mx.clients.domain.entity.Client

class ClientAdapter(
    private val onRowClick: (Client) -> Unit
) : ListAdapter<Client, ClientViewHolder>(ClientDiffUtil()) {

    /** */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientViewHolder =
        ClientViewHolder.from(parent)

    /** */
    override fun onBindViewHolder(holder: ClientViewHolder, position: Int) {
        val client = getItem(position)
        holder.bind(client = client, onRowClick = onRowClick)
    }

    /** */
    internal class ClientDiffUtil : DiffUtil.ItemCallback<Client>() {
        /** */
        override fun areItemsTheSame(
            oldItem: Client,
            newItem: Client
        ): Boolean = oldItem == newItem

        /** */
        override fun areContentsTheSame(
            oldItem: Client,
            newItem: Client
        ): Boolean = oldItem == newItem

    }
}