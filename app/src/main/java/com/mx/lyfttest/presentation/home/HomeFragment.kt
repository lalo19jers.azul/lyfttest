package com.mx.lyfttest.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.mx.clients.domain.entity.Client
import com.mx.clients.presentation.getClients.GetClientsStatus
import com.mx.domain.Status
import com.mx.lyfttest.R
import com.mx.lyfttest.databinding.HomeFragmentBinding
import com.mx.lyfttest.presentation.common.extensions.hideProgressBar
import com.mx.lyfttest.presentation.common.extensions.presentShortSnackBar
import com.mx.lyfttest.presentation.common.extensions.showProgressBar
import com.mx.lyfttest.presentation.home.adapter.ClientAdapter
import kotlinx.serialization.json.JsonArray
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class HomeFragment : Fragment() {

    /* */
    private val binding: HomeFragmentBinding
            by lazy { HomeFragmentBinding.inflate(layoutInflater) }

    /* */
    private val viewModel: HomeViewModel by viewModel()

    /* */
    private val clientAdapter: ClientAdapter
    by lazy { ClientAdapter(onRowClick = onRowClick) }

    /** */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        execute()
    }

    /** */
    private fun execute() {
        viewModel.getClientsAsLiveData().observe(
            viewLifecycleOwner, handleClientsStatusObserver()
        )
    }

    /** */
    private fun handleClientsStatusObserver() = Observer<GetClientsStatus> {
        requireContext().hideProgressBar()
        when (it) {
            is Status.Loading -> requireContext().showProgressBar()
            is Status.Error -> binding.root.presentShortSnackBar(it.failure.toString())
            is Status.Done -> setUpView(clients = it.value.clients)
        }
    }

    /** */
    private fun setUpView(clients: List<Client>) {
        binding.apply {
            acbCreateClient.setOnClickListener {
                findNavController().navigate(R.id.action_homeFragment_to_createClientFragment)
            }
            clientAdapter.submitList(clients)
            rvClients.adapter = clientAdapter
        }
    }

    /** */
    private val onRowClick: (Client) -> Unit = {
      binding.root.presentShortSnackBar("Hi ${it.name}")
    }




}