package com.mx.lyfttest.presentation.addClient

import androidx.lifecycle.ViewModel
import com.mx.clients.presentation.addClient.AddClient

class CreateClientViewModel(
    addClient: AddClient
) : ViewModel() , AddClient by addClient