package com.mx.lyfttest

import android.app.Application
import com.mx.lyfttest.di.initKoin
import timber.log.Timber

class LyftApp: Application() {

    /** */
    override fun onCreate() {
        super.onCreate()
        initKoin()
        Timber.plant(Timber.DebugTree())
    }

}