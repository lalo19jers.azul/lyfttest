package com.mx.clients.domain.entity

data class Client(
    val id: String,
    val name: String,
    val lastName: String?,
    val email: String?,
    val phone: String?,
    val address: String?,
    val creationDate: String?,
    val externalId: String?,
    val clabe: String?
)
