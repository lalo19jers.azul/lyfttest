package com.mx.clients.domain.useCase.addClient

data class AddClientParams(
    val name: String,
    val lastName: String,
    val email: String,
    val phoneNumber: String
)
