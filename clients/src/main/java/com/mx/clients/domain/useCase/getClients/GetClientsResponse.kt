package com.mx.clients.domain.useCase.getClients

import com.mx.clients.domain.entity.Client
import kotlinx.serialization.json.JsonArray

data class GetClientsResponse(
    val clients: List<Client>
)