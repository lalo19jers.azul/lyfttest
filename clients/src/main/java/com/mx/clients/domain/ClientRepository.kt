package com.mx.clients.domain

import com.mx.clients.domain.useCase.addClient.AddClientFailure
import com.mx.clients.domain.useCase.addClient.AddClientParams
import com.mx.clients.domain.useCase.addClient.AddClientResponse
import com.mx.clients.domain.useCase.getClients.GetClientsFailure
import com.mx.clients.domain.useCase.getClients.GetClientsResponse
import com.mx.domain.Either

interface ClientRepository {

    /** */
    suspend fun getClients(): Either<GetClientsFailure, GetClientsResponse>

    /** */
    suspend fun addClient(
        params: AddClientParams
    ): Either<AddClientFailure, AddClientResponse>
}