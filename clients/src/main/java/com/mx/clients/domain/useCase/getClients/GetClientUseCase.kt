package com.mx.clients.domain.useCase.getClients

import com.mx.clients.domain.ClientRepository
import com.mx.domain.Either
import com.mx.domain.UseCase

class GetClientUseCase(
    private val repository: ClientRepository
): UseCase<GetClientsResponse, GetClientsParams, GetClientsFailure>() {

    /** */
    override suspend fun run(
        params: GetClientsParams
    ): Either<GetClientsFailure, GetClientsResponse> =
        repository.getClients()


}