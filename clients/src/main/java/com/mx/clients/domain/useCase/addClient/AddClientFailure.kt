package com.mx.clients.domain.useCase.addClient

import com.mx.datasource.failureManage.HttpFailure
import com.mx.domain.Failure


sealed class AddClientFailure : Failure.FeatureFailure() {
    /* */
    object NetworkConnectionFailure : AddClientFailure()

    /* */
    object NoClientsFound : AddClientFailure()

    /* */
    object NullPointerException: AddClientFailure()

    /* */
    data class JsonDataDeserializationFailure(val message: String?) : AddClientFailure()

    /* */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : AddClientFailure(), HttpFailure

    /* */
    data class UnknownFailure(val message: String?) : AddClientFailure()

    /** */
    companion object {
        internal fun fromFeatureFailure(failure: Failure) = when (failure) {
            is AddClientFailure -> failure
            else -> UnknownFailure(failure.javaClass.simpleName)
        }
    }
}