package com.mx.clients.domain.useCase.getClients

import com.mx.datasource.failureManage.HttpFailure
import com.mx.domain.Failure

sealed class GetClientsFailure : Failure.FeatureFailure() {
    /* */
    object NetworkConnectionFailure : GetClientsFailure()

    /* */
    object NoClientsFound : GetClientsFailure()

    /* */
    object NullPointerException: GetClientsFailure()

    /* */
    data class JsonDataDeserializationFailure(val message: String?) : GetClientsFailure()

    /* */
    data class ServerFailure(
        override val code: Int,
        override val message: String
    ) : GetClientsFailure(), HttpFailure

    /* */
    data class UnknownFailure(val message: String?) : GetClientsFailure()

    /** */
    companion object {
        internal fun fromFeatureFailure(failure: Failure) = when (failure) {
            is GetClientsFailure -> failure
            else -> UnknownFailure(failure.javaClass.simpleName)
        }
    }
}