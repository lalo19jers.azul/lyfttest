package com.mx.clients.domain.useCase.addClient

import com.mx.clients.domain.ClientRepository
import com.mx.domain.Either
import com.mx.domain.UseCase

class AddClientUseCase(
    private val repository: ClientRepository
): UseCase<AddClientResponse, AddClientParams, AddClientFailure>() {

    /** */
    override suspend fun run(
        params: AddClientParams
    ): Either<AddClientFailure, AddClientResponse> =
        repository.addClient(params)


}