package com.mx.clients.data.dataSource.remote

import com.mx.clients.data.dataSource.ClientRemoteDataSource
import com.mx.clients.data.dataSource.remote.failure.toAddClientFailure
import com.mx.clients.data.dataSource.remote.failure.toClientFailure
import com.mx.clients.domain.entity.Client
import com.mx.clients.domain.useCase.addClient.AddClientFailure
import com.mx.clients.domain.useCase.addClient.AddClientParams
import com.mx.clients.domain.useCase.addClient.AddClientResponse
import com.mx.clients.domain.useCase.getClients.GetClientsFailure
import com.mx.clients.domain.useCase.getClients.GetClientsResponse
import com.mx.datasource.remote.model.retrofitApiCall
import com.mx.domain.Either
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonObject
import retrofit2.HttpException

internal class ClientRemoteDataSourceImpl(
    private val apiService: ClientApiService
) : ClientRemoteDataSource {

    /** */
    override suspend fun getClients(): Either<GetClientsFailure, GetClientsResponse> =
        try {
            retrofitApiCall {
                apiService.getClients()
            }.let { clients ->
                val listOfClients = arrayListOf<Client>().apply {
                    for (i in 0 until clients.size) {
                        val jsonClient = clients[i].jsonObject
                        add(getClientFromJsonObject(jsonClient))
                    }
                }
                Either.Right(GetClientsResponse(clients = listOfClients))
            }
        } catch (e: Exception) {
            val failure = when (e) {
                is HttpException -> e.toClientFailure()
                is SerializationException -> GetClientsFailure.JsonDataDeserializationFailure(e.message)
                else -> GetClientsFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }

    override suspend fun addClient(
        params: AddClientParams
    ): Either<AddClientFailure, AddClientResponse> =
        try {
            val requestBody: MutableMap<String, String> = HashMap()
            requestBody["email"] = params.email
            requestBody["last_name"] = params.lastName
            requestBody["name"] = params.name
            requestBody["phone_number"] = params.phoneNumber

            retrofitApiCall {
                apiService.addClient(
                    clientRequestBody = requestBody
                )
            }.let { Either.Right(AddClientResponse) }
        } catch (e: Exception) {
            val failure = when (e) {
                is HttpException -> e.toAddClientFailure()
                is SerializationException -> AddClientFailure.JsonDataDeserializationFailure(e.message)
                else -> AddClientFailure.UnknownFailure(e.message)
            }
            Either.Left(failure)
        }

    /** */
    private fun getClientFromJsonObject(jsonClient: JsonObject): Client =
        Client(
            id = jsonClient["id"].toString(),
            name = jsonClient["name"].toString(),
            lastName = jsonClient["last_name"].toString() ?: null,
            email = jsonClient["email"].toString() ?: null,
            phone = jsonClient["phone_number"].toString() ?: null,
            address = jsonClient["address"].toString() ?: null,
            creationDate = jsonClient["creation_date"].toString() ?: null,
            externalId = jsonClient["external_id"].toString() ?: null,
            clabe = jsonClient["clabe"].toString() ?: null
        )

}
