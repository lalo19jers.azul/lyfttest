package com.mx.clients.data.dataSource.remote.failure

import com.mx.clients.domain.useCase.addClient.AddClientFailure
import com.mx.clients.domain.useCase.getClients.GetClientsFailure
import com.mx.datasource.remote.model.HttpErrorCode
import retrofit2.HttpException

/** */
internal fun HttpException.toClientFailure(): GetClientsFailure =
    when(HttpErrorCode.from(code())) {
        HttpErrorCode.HTTP_NOT_FOUND -> GetClientsFailure.NoClientsFound
        else -> GetClientsFailure.ServerFailure(code(),message())
    }

/** */
internal fun HttpException.toAddClientFailure(): AddClientFailure =
    when(HttpErrorCode.from(code())) {
        HttpErrorCode.HTTP_NOT_FOUND -> AddClientFailure.NoClientsFound
        else -> AddClientFailure.ServerFailure(code(),message())
    }