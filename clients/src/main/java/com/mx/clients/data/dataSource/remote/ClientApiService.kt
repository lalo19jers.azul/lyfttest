package com.mx.clients.data.dataSource.remote

import com.mx.clients.data.dataSource.remote.model.AddClientRequest
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

internal interface ClientApiService {

    /** */
    @Headers("Content-Type: application/json")
    @GET(URL.GET_CLIENTS)
    suspend fun getClients(): Response<JsonArray>

    /** */
    @Headers("Content-Type: application/json")
    @POST(URL.GET_CLIENTS)
    suspend fun addClient(@Body clientRequestBody:  Map<String, String>): Response<Unit>

    private object URL {
        const val GET_CLIENTS = "/v1/$MERCHANT_ID/customers "

    }

    companion object {
        const val MERCHANT_ID = "m422opgx6l4gcf4xf7t7"
    }


}