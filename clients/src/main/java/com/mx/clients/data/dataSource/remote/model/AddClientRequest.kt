package com.mx.clients.data.dataSource.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AddClientRequest(
    @SerialName(value = "name") val name: String,
    @SerialName(value = "last_name") val lasName: String,
    @SerialName(value = "email") val email: String,
    @SerialName(value = "phone_number") val phone: String,
    @SerialName(value = "requires_account") val requiresAccount: Boolean = false,
)