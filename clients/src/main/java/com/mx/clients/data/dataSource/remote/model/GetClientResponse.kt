package com.mx.clients.data.dataSource.remote.model

import com.mx.clients.data.dataSource.remote.model.dto.ClientDto
import kotlinx.serialization.Serializable

@Serializable
data class GetClientResponse(
    val list: ArrayList<ClientDto>
)