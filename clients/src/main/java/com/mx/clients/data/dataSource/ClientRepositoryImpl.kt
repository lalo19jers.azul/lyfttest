package com.mx.clients.data.dataSource

import com.mx.clients.domain.ClientRepository
import com.mx.clients.domain.useCase.addClient.AddClientFailure
import com.mx.clients.domain.useCase.addClient.AddClientParams
import com.mx.clients.domain.useCase.addClient.AddClientResponse
import com.mx.clients.domain.useCase.getClients.GetClientsFailure
import com.mx.clients.domain.useCase.getClients.GetClientsResponse
import com.mx.domain.Either
import com.mx.network.internetConnection.InternetConnectionRepository

internal class ClientRepositoryImpl(
    private val clientRemoteDataSource: ClientRemoteDataSource,
    internetConnectionRepository: InternetConnectionRepository
) : ClientRepository, InternetConnectionRepository by internetConnectionRepository {

    /** */
    override suspend fun getClients():
            Either<GetClientsFailure, GetClientsResponse> =
        clientRemoteDataSource.getClients()

    /** */
    override suspend fun addClient(
        params: AddClientParams
    ): Either<AddClientFailure, AddClientResponse> =
        clientRemoteDataSource.addClient(params = params)


}