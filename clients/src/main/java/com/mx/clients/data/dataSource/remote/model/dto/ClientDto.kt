package com.mx.clients.data.dataSource.remote.model.dto

import com.mx.clients.domain.entity.Client
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class ClientDto(
    @SerialName(value = "id") val id: String,
    @SerialName(value = "name") val name: String,
    @SerialName(value = "last_name") val lastName: String?,
    @SerialName(value = "email") val email: String,
    @SerialName(value = "phone_number") val phone: String?,
    @SerialName(value = "address") val address: String?,
    @SerialName(value = "creation_date") val creationDate: String?,
    @SerialName(value = "external_id") val externalId: String?,
    @SerialName(value = "clabe") val clabe: String?
) {

    /** */
    fun toClient(): Client =
        Client(
            id = id,
            name = name,
            lastName = lastName,
            email = email,
            phone = phone,
            address = address,
            creationDate = creationDate,
            externalId = externalId,
            clabe = clabe
        )
}