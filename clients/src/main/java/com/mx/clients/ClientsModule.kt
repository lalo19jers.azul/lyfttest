package com.mx.clients

import com.mx.clients.data.dataSource.ClientRemoteDataSource
import com.mx.clients.data.dataSource.ClientRepositoryImpl
import com.mx.clients.data.dataSource.remote.ClientApiService
import com.mx.clients.data.dataSource.remote.ClientRemoteDataSourceImpl
import com.mx.clients.domain.ClientRepository
import com.mx.clients.domain.useCase.addClient.AddClientUseCase
import com.mx.clients.domain.useCase.getClients.GetClientUseCase
import com.mx.clients.presentation.addClient.AddClient
import com.mx.clients.presentation.addClient.AddClientImpl
import com.mx.clients.presentation.getClients.GetClients
import com.mx.clients.presentation.getClients.GetClientsImpl
import org.koin.dsl.module
import retrofit2.Retrofit


val clientsModule = module {

    /** PRESENTATION */
    single<GetClients> { GetClientsImpl(getClientUseCase = get()) }
    single< AddClient> { AddClientImpl(addClientUseCase = get()) }

    /** USE CASE */
    factory { GetClientUseCase(repository = get()) }
    factory { AddClientUseCase(repository = get()) }

    /** REPOSITORY */
    single<ClientRepository> {
        ClientRepositoryImpl(
            clientRemoteDataSource = get(),
            internetConnectionRepository = get()
        )
    }

    /** DATA SOURCE REMOTE */
    single<ClientRemoteDataSource> {
        ClientRemoteDataSourceImpl(
            apiService = get()
        )
    }

    /** API SOURCE */
    single { get<Retrofit>().create(ClientApiService::class.java) }

}