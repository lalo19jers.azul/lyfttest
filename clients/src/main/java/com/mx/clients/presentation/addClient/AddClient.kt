package com.mx.clients.presentation.addClient

import androidx.lifecycle.LiveData
import com.mx.clients.domain.useCase.addClient.AddClientFailure
import com.mx.clients.domain.useCase.addClient.AddClientParams
import com.mx.clients.domain.useCase.addClient.AddClientResponse
import com.mx.domain.Status

/** */
typealias AddClientStatus =
        Status<AddClientFailure, AddClientResponse>

interface AddClient {

    /** */
    fun addClientAsLiveData(
        params: AddClientParams
    ): LiveData<AddClientStatus>

}