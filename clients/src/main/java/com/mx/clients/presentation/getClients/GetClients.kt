package com.mx.clients.presentation.getClients

import androidx.lifecycle.LiveData
import com.mx.clients.domain.useCase.getClients.GetClientsFailure
import com.mx.clients.domain.useCase.getClients.GetClientsResponse
import com.mx.domain.Either
import com.mx.domain.Status

/** */
typealias GetClientsStatus =
        Status<GetClientsFailure, GetClientsResponse>

interface GetClients {

    /* */
    var getClientsResponse: GetClientsResponse

    /* */
    var getClientsFailure: GetClientsFailure

    /** */
    fun getClientsAsLiveData(): LiveData<GetClientsStatus>

    /** */
    suspend fun getClientsAsEither(): Either<GetClientsFailure, GetClientsResponse>
}