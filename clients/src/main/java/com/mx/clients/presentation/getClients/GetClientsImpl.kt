package com.mx.clients.presentation.getClients

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.mx.clients.domain.useCase.getClients.GetClientUseCase
import com.mx.clients.domain.useCase.getClients.GetClientsFailure
import com.mx.clients.domain.useCase.getClients.GetClientsParams
import com.mx.clients.domain.useCase.getClients.GetClientsResponse
import com.mx.domain.Either
import com.mx.domain.Status
import com.mx.domain.onFailure
import com.mx.domain.onRight
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow

class GetClientsImpl(
    private val getClientUseCase: GetClientUseCase
) : GetClients {

    /* */
    override lateinit var getClientsResponse: GetClientsResponse

    /* */
    override lateinit var getClientsFailure: GetClientsFailure

    /** */
    override fun getClientsAsLiveData(): LiveData<GetClientsStatus> = flow<GetClientsStatus> {
        emit(Status.Loading())
        getClientsAsEither()
            .onFailure { emit(Status.Error(it)) }
            .onRight {  emit(Status.Done(it))  }
    }.asLiveData(Dispatchers.IO)

    /** */
    override suspend fun getClientsAsEither():
            Either<GetClientsFailure, GetClientsResponse> =
        getClientUseCase.run(GetClientsParams)
            .onFailure { getClientsFailure = it }
            .onRight { getClientsResponse = it }

}