package com.mx.clients.presentation.addClient

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.mx.clients.domain.useCase.addClient.AddClientParams
import com.mx.clients.domain.useCase.addClient.AddClientUseCase
import com.mx.domain.Status
import com.mx.domain.onFailure
import com.mx.domain.onRight
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow

class AddClientImpl(
    private val addClientUseCase: AddClientUseCase
) : AddClient {
    /** */
    override fun addClientAsLiveData(
        params: AddClientParams
    ): LiveData<AddClientStatus> = flow<AddClientStatus> {
        emit(Status.Loading())
        addClientUseCase.run(params)
            .onFailure { emit(Status.Error(it)) }
            .onRight { emit(Status.Done(it)) }
    }.asLiveData(Dispatchers.IO)


}