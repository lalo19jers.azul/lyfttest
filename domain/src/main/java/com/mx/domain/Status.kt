package com.mx.domain

sealed class Status<F: Failure, T> {

    /**
     *
     *
     */
    class Loading<F: Failure, T> : Status<F, T>()

    /**
     *
     *
     */
    data class Error<F: Failure, T>(
        val failure: F
    ) : Status<F, T>()

    /**
     *
     * @param value
     */
    data class Done<F: Failure, T>(
        val value: T
    ) : Status<F, T>()

}