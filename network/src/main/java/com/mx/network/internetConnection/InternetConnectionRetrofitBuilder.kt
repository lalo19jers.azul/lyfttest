package com.mx.network.internetConnection

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

internal class InternetConnectionRetrofitBuilder {

    /* */
    private val baseUrl: String = "https://clients3.google.com/"
    /* */
    private val timeOut: Long = 500L


    /**
     *
     * @return [Retrofit]
     */
    fun build() : Retrofit =
        Retrofit.Builder()
            .client(buildHttpClient())
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl(baseUrl)
            .build()

    /**
     *
     * @return [OkHttpClient]
     */
    private fun buildHttpClient() : OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .build()

}