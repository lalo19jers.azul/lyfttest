package com.mx.datasource.exception

/** */
fun Exception.message(): String = message ?: tag

/** */
val Exception.tag: String
    get() = "${javaClass.simpleName} - $message"