package com.mx.datasource.remote.httpClient

import android.util.Base64

class HeaderInterceptorImpl: HeaderInterceptor {

    /**
     * @return [String]
     */
    override fun getAuthorizationType(): String = BASIC_AUTH_VALUE

    /**
     * @return [String]
     */
    override fun getAuthorizationValue(): String {
        return AUTH_VALUE
       // return Base64.encodeToString(AUTH_VALUE.toByteArray(), Base64.NO_WRAP)
    }
    companion object {
        const val BASIC_AUTH_VALUE = "Basic"
        const val AUTH_VALUE = "sk_f3db4a444133439b92f2df7a33b5a02a"
    }

}