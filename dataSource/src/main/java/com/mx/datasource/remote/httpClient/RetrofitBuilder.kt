package com.mx.datasource.remote.httpClient

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.mx.datasource.remote.httpClient.RetrofitBuilder.CONSTANTS.CONTENT_TYPE
import kotlinx.serialization.json.Json
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.io.IOException
import java.util.concurrent.TimeUnit

class RetrofitBuilder(
    private val baseUrl: String,
    private val headerInterceptor: HeaderInterceptor? = null
) {

    /**
     *  Logging interceptor. By default, Retrofit doesn't log any request
     */
    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    /* */
    private val contentType = CONTENT_TYPE.toMediaType()

    /** */
    private val timeOut = 100L

    /**
     * Build Retrofit instance
     * @return [Retrofit]
     */
    fun build(): Retrofit =
        Retrofit.Builder().client(buildHttpClient())
            .baseUrl(baseUrl)
            .addConverterFactory(Json {
                ignoreUnknownKeys = true
            }.asConverterFactory(contentType))
            .build()

    /**
     * @return [OkHttpClient]
     */
    private fun buildHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .addInterceptor(getBasicInterceptor())
            .apply { addInterceptor(loggingInterceptor) }
            .build()


    /**
     * @return [Interceptor]
     */
    private fun getBasicInterceptor() = BasicAuthInterceptor(
       headerInterceptor?.getAuthorizationValue() ?: "",
        ""
    )

    /** */
    private object CONSTANTS {
        /* */
        const val CONTENT_TYPE = "application/json"
    }

}

class BasicAuthInterceptor(user: String, password: String) :
    Interceptor {
    private val credentials: String = Credentials.basic(user, password)

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val authenticatedRequest: Request = request.newBuilder()
            .header("Authorization", credentials).build()
        return chain.proceed(authenticatedRequest)
    }

}